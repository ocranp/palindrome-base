package sheridan;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindrome( ) {
		
		
		assertTrue("Not a palindrome", Palindrome.isPalindrome("anna"));
	}

	@Test
	public void testIsPalindromeNegative( ) {
		
		assertFalse(Palindrome.isPalindrome("this is not palindrome"));
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		assertTrue(Palindrome.isPalindrome("aa") );
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		assertFalse("Must match case", Palindrome.isPalindrome("Anna"));
	}	
	
}
